<?php
require_once __DIR__."/../vendor/autoload.php";
use app\Router;
use app\controller\ProductController;
$router = new Router();
$router->get('/',[ProductController::class,'index']);
$router->get('/product',[ProductController::class,'index']);// alise of product
$router->get('/product/create',[ProductController::class,'create']);
$router->post('/product/create',[ProductController::class,'create']);
$router->get('/product/update',[ProductController::class,'update']);
$router->post('/product/update',[ProductController::class,'update']);
//$router->get('/product/delete',[ProductController::class,'delete']);
$router->post('/product/delete',[ProductController::class,'delete']);

$router->resolve();