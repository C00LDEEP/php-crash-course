<?php
namespace app;
use PDO;

class Database {
    public \PDO $pdo;
    public static $db;
    public function __construct(){
        $this->pdo = new PDO('mysql:host=localhost;port=3306;dbname=products_crud','root','');
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);   
        self::$db=$this;     
    }

    public function getProducts($search='')
    {
        if($search){
            $statement = $this->pdo->prepare('select * from products where title like :search order by created_date desc');
            $statement->bindValue(':search',"%".$search."%");
        }else{
            $statement = $this->pdo->prepare('select * from products order by created_date desc');
        }
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);        
    }

    public function createProduct($product){
        $statement = $this->pdo->prepare("insert into products(title,description,image,price,created_date) values(:title,:description,:image,:price,:date)");
        $statement->bindValue(':title',$product->title);
        $statement->bindValue(':description',$product->description);
        $statement->bindValue(':price',$product->price);
        $statement->bindValue(':image',$product->imagePath);
        $statement->bindValue(':date',date('Y-m-d H:i:s'));
        $statement->execute();        
    }
    public function updateProduct($product){
        $statement = $this->pdo->prepare("update products set title=:title,description=:description,image=:image,price=:price where id=:id");
        $statement->bindValue(':title',$product->title);
        $statement->bindValue(':description',$product->description);
        $statement->bindValue(':price',$product->price);
        $statement->bindValue(':image',$product->imagePath);
        $statement->bindValue(':id',$product->id);
        $statement->execute();
    }

    public function getProductById($id){

        $statement = $this->pdo->prepare('select * from products where id=:id');
        $statement->bindValue(':id',$id);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);        
    }

    public function deleteProduct($id){
        $statement = $this->pdo->prepare('delete from products where id=:id');
        $statement->bindValue(':id',$id);
        $statement->execute();
    }
}