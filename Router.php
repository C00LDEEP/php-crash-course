<?php
namespace app;


class Router {

    public $getRoutes  = [];
    public $postRoutes = [];
    public Database $db;
    
    function __construct(){
        $this->db = new Database();
    }

    public function get($url, $fun){
        $this->getRoutes[$url] = $fun;
    }

    public function post($url, $fun){
        $this->postRoutes[$url] = $fun;
    }
    public function resolve(){
        $currentUrl = $_SERVER['REQUEST_URI'] ?? '/';
        if(strpos($currentUrl,'?') !== false){
            $currentUrl = substr($currentUrl,0,strpos($currentUrl,'?'));
        }
        $method = $_SERVER['REQUEST_METHOD'] ?? '/';        

        if($method == 'GET'){
            $fn = $this->getRoutes[$currentUrl]?? null;
        } else{
            $fn = $this->postRoutes[$currentUrl]?? null;            
        }

        if($fn){
            call_user_func($fn,$this);
        }else{
            echo "Page Not Found";
        }
    }

    public function renderView($view,$params=[]){ // $view will be product/index(
        foreach ($params as $key => $value) {
            $$key = $value;
        }
        ob_start();
        include_once __DIR__."/views/$view.php";
        $content = ob_get_clean();
        include_once __DIR__."/views/_layout.php";
    }        

}