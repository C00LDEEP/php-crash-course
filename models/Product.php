<?php
namespace app\models;
use app\Database;

class Product {
    public ?int $id =null;
    public ?string $title=null;
    public ?string $description=null;
    public ?float $price=null;
    public ?string $imagePath=null;
    public ?array $imageFile=null;

    public function load($data){
        $this->id = $data['id']??null;
        $this->title = $data['title'];
        $this->description = $data['description'];
        $this->price = (float)$data['price'];
        $this->imageFile = $data['imageFile'] ?? null;
        $this->imagePath = $data['image'] ?? null;
    }

    public function save(){
        $errors = [];
        if(!$this->title){
            $errors[]='Product title is required';
        }
        if(!$this->description){
            $errors[]='Product description is required';
        }        
        if(!$this->price){
            $errors[]='Product price is required';
        }  

        if(!is_dir(__DIR__.'/../public/images')){
            mkdir(__DIR__.'/../public/images');
        }     
        //$this->imagePath = $image2;    
        if(empty($errors))
        {
            if($this->imageFile && $this->imageFile['tmp_name']){
                ($this->imagePath)? unlink(__DIR__.'/../public'.$this->imagePath):'';
                $folder = time();
                $this->imagePath ='/images/'.$folder.'/'.$this->imageFile['name'];
                $path=dirname('/public'.$this->imagePath);
                mkdir(__DIR__.'/..'.$path);
                move_uploaded_file($this->imageFile['tmp_name'],__DIR__.'/../public'.$this->imagePath);
            }

            $db= Database::$db;
            if($this->id){
                $db->updateProduct($this);
            }else{
                $db->createProduct($this);
            }
        } 
        

        return $errors;
    }

}