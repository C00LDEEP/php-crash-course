<?php
namespace app\controller;
use app\models\Product;

class ProductController 
{
    public function index($router){
        $search = $_GET['search'] ?? '';
        $products=$router->db->getProducts($search);

        $router->renderView('products/index',[
            'products' => $products,
            'search'=> $search,
        ]);
    }     

    public function create($router){
        $errors = [];
        $productData = [
            'title' => '',
            'description' => '',
            'price' => '',
            'image_src' => null
        ];

        if($_SERVER['REQUEST_METHOD'] === "POST"){
            $productData['title'] = $_POST['title'];
            $productData['description'] = $_POST['description'];
            $productData['price'] = $_POST['price'];
            $productData['imageFile'] = $_FILES['image']?? null;

            $product = new Product();
            $product->load($productData);
            $errors = $product->save();
            if(empty($errors)){
                header('Location: /product');exit;      
            }      
        }

        
        $router->renderView('products/create',[
            'product' => $productData,
            'errors'=> $errors,
        ]);        
    }     

    public function update($router){
        $id=$_GET['id']??null;
        if(!$id){
            header('Location: /product'); exit;
        }
        $errors=[];

        $productData = $router->db->getProductById($id);       

        if($_SERVER['REQUEST_METHOD'] === "POST"){
            $productData['title'] = $_POST['title'];
            $productData['description'] = $_POST['description'];
            $productData['price'] = $_POST['price'];
            $productData['imageFile'] = $_FILES['image']?? null;

            $product = new Product();
            $product->load($productData);
            $errors = $product->save();

            if(empty($errors)){
                header('Location: /product');exit;      
            }      
        }        

        $router->renderView('products/update',[
            'product' => $productData,
            'errors'=> $errors,
        ]);              
    }     
    public function delete($router){
        $id =$_POST['did'] ?? null;
        if(!$id){
            header('Location: /product'); exit;
        }
        $router->db->deleteProduct($id);
        header("Location:/product");        
    }                   
}