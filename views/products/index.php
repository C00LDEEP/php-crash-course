
<h1>Product List</h1>
<p> 
    <a href='/product/create' class="btn btn-success">Create Product</a>        
    </p>

    <form action="">
        <div class="input-group mb-3">
            <input type="text" value="<?php echo $search; ?>" placeholder="Search for products" name="search" class="form-control">
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">Search</button>
            </div>
        </div>
    </form>

    <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Image</th>
      <th scope="col">Title</th>
      <th scope="col">Price</th>
      <th scope="col">Created Date</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php foreach($products as $i => $product): ?>
    <tr>
      <th scope="row"><?php echo $i+1;?></th>
      <td><img src="<?php echo $product['image']; ?>" class="thumb-image"></td>
      <td><?php echo $product['title'];?></td>      
      <!-- <td><?php //echo $product['description'];?></td>       -->
      <td><?php echo $product['price'];?></td>      
      <td><?php echo $product['created_date'];?></td>      
      <td><a href="/product/update?id=<?php echo $product['id']; ?> " class="btn btn-primary">Edit</a>
      <form style="display:inline-block" action='/product/delete' method="post">
          <input type="hidden" name="did" value="<?php echo $product['id']; ?>">
        <button type="submit" class='btn btn-danger'>Delete</button>
      </form>
    </td>      
    </tr>
    <?php endforeach; ?>
  </tbody>
</table> 