<?php if(!empty($errors)): ?>
    <div class="alert alert-danger">
        <?php foreach ($errors as $value) :?>
            <div><?php echo $value; ?></div>
        <?php  endforeach; ?>
    </div>
    <?php endif; ?>
<form action=" " method="post" enctype="multipart/form-data">
    <?php if($product['image']): ?>
        <img class="update-image" src="<?php echo $product['image']; ?>">
    <?php endif; ?>
  <div class="mb-3">
    <label class="form-label">Product Image</label>
    <input type="file" name="image" class="form-control">
  </div>
  <div class="mb-3">
    <label class="form-label">Product Title</label>
    <input type="text" name="title" value="<?php echo $product['title']; ?>" class="form-control" aria-describedby="productHelp">
    <div id="productHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>  
  <div class="mb-3">
    <label class="form-label">Product Description</label>
    <textarea class="form-control" name="description"><?php echo $product['description']; ?></textarea>
  </div>
  <div class="mb-3">
    <label class="form-label">Product Price</label>
    <input type="number" step=".01" name="price" value="<?php echo $product['price']; ?>" class="form-control">
  </div>
  
  <button type="submit" class="btn btn-primary">Save</button>
</form>    